<?php

namespace Drupal\rets;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of RETS Query entities.
 */
class RetsQueryListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('RETS Query');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $operations['test_query'] = [
      'title' => t('Test query'),
      'url' => $entity->toUrl('test-form'),
    ];
    $operations['data'] = [
      'title' => $this->t('Data mappings'),
      'url' => $entity->toUrl('data-form'),
    ];
    $operations['media'] = [
      'title' => $this->t('Media settings'),
      'url' => $entity->toUrl('media-form'),
    ];
    $operations['scheduling'] = [
      'title' => $this->t('Scheduling'),
      'url' => $entity->toUrl('schedule-form')
    ];
    return $operations;  }

}
