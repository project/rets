<?php

namespace Drupal\rets\Plugin\QueueWorker;

/**
 * A RETS content import queue worker.
 *
 * @QueueWorker(
 *   id = "rets_content_delete",
 *   title = @Translation("Queue worker for RETS media"),
 *   cron = {"time" = 300}
 * )
 *
 * @package Drupal\rets\Plugin\QueueWorker
 */
class RetsContentDeleteQueueWorker extends RetsImportQueueBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    if (!$this->importManager->deleteContent($item->entity)) {
      throw new \Exception('Unable to delete entity');
    }
  }

}
