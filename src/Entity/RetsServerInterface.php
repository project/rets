<?php

namespace Drupal\rets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining RETS Server entities.
 */
interface RetsServerInterface extends ConfigEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getLoginUrl();

  /**
   * {@inheritdoc}
   */
  public function getUsername();

  /**
   * {@inheritdoc}
   */
  public function getPassword();

  /**
   * {@inheritdoc}
   */
  public function getRetsVersion();

  /**
   * {@inheritdoc}
   */
  public function getUserAgent();

  /**
   * {@inheritdoc}
   */
  public function getUserAgentPassword();

  /**
   * {@inheritdoc}
   */
  public function getHttpAuthentication();

  /**
   * {@inheritdoc}
   */
  public function getUsePostMethod();

  /**
   * {@inheritdoc}
   */
  public function getDisableFollowLocation();

  /**
   * Gets the valid rets versions from PHRETS.
   *
   * @return string[]
   */
  public function getAvailableRetsVersions();

  /**
   * {@inheritdoc}
   */
  public function setLoginUrl($login_url);

  /**
   * {@inheritdoc}
   */
  public function setUsername($username);

  /**
   * {@inheritdoc}
   */
  public function setPassword($password);

  /**
   * {@inheritdoc}
   */
  public function setRetsVersion($rets_version);

  /**
   * {@inheritdoc}
   */
  public function setUserAgent($user_agent);

  /**
   * {@inheritdoc}
   */
  public function setUserAgentPassword($user_agent_password);

  /**
   * {@inheritdoc}
   */
  public function setHttpAuthentication($http_authentication);

  /**
   * {@inheritdoc}
   */
  public function setUsePostMethod($use_post_method);

  /**
   * {@inheritdoc}
   */
  public function setDisableFollowLocation($disable_follow_location);

}
