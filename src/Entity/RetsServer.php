<?php

namespace Drupal\rets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the RETS Server entity.
 *
 * @ConfigEntityType(
 *   id = "rets_server",
 *   label = @Translation("RETS Server"),
 *   label_singular = @Translation("rets server"),
 *   label_plural = @Translation("rets servers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count rets server",
 *     plural = "@count rets servers"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rets\RetsServerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\rets\Form\RetsServerForm",
 *       "edit" = "Drupal\rets\Form\RetsServerForm",
 *       "delete" = "Drupal\rets\Form\RetsServerDeleteForm",
 *       "queries" = "Drupal\rets\RetsQueryListBuilder"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\rets\RetsServerHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "rets_server",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/rets/rets_servers/{rets_server}",
 *     "add-form" = "/admin/config/rets/rets_servers/add",
 *     "edit-form" = "/admin/config/rets/rets_servers/{rets_server}/edit",
 *     "delete-form" = "/admin/config/rets/rets_servers/{rets_server}/delete",
 *     "collection" = "/admin/config/rets/rets_servers",
 *     "queries" = "/admin/config/rets/rets_servers/{rets_server}/queries"
 *   }
 * )
 */
class RetsServer extends ConfigEntityBase implements RetsServerInterface {

  /**
   * The RETS Server ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The RETS Server label.
   *
   * @var string
   */
  protected $label;


  /**
   * The RETS Server Login URL.
   *
   * @var string
   */
  protected $login_url;

  /**
   * The RETS Server User Name.
   *
   * @var string
   */
  protected $username;

  /**
   * The RETS Server Password.
   *
   * @var string
   */
  protected $password;

  /**
   * The RETS Server Version.
   *
   * @var string
   */
  protected $rets_version;

  /**
   * The RETS Server User Agent.
   *
   * @var string
   */
  protected $user_agent;

  /**
   * The RETS Server User Agent Password.
   *
   * @var string
   */
  protected $user_agent_password;

  /**
   * The HTTP Authentication type.
   *
   * @var string
   */
  protected $http_authentication;

  /**
   * The Use POST Method.
   *
   * @var boolean
   */
  protected $use_post_method;

  /**
   * The Disable Follow Location.
   *
   * @var string
   */
  protected $disable_follow_location;


  /**
   * {@inheritdoc}
   */
  public function getLoginUrl() {
    return $this->login_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * {@inheritdoc}
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * {@inheritdoc}
   */
  public function getRetsVersion() {
    return $this->rets_version;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserAgent() {
    return $this->user_agent;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserAgentPassword() {
    return $this->user_agent_password;
  }

  /**
   * {@inheritdoc}
   */
  public function getHttpAuthentication() {
    return $this->http_authentication;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsePostMethod() {
    return $this->use_post_method;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisableFollowLocation() {
    return $this->disable_follow_location;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableRetsVersions() {
    $versions = [];
    $phrets_version = new \PHRETS\Versions\RETSVersion();
    $values = array_values($phrets_version->getValidVersions());
    foreach ($values as $value) {
      $versions[$value] = $value;
    }
    return $versions;
  }

  /**
   * {@inheritdoc}
   */
  public function setLoginUrl($login_url) {
    $this->login_url = $login_url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUsername($username) {
    $this->username = $username;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPassword($password) {
    $this->password = $password;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setRetsVersion($rets_version) {
    $this->rets_version = $rets_version;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserAgent($user_agent) {
    $this->user_agent = $user_agent;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserAgentPassword($user_agent_password) {
    $this->user_agent_password = $user_agent_password;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHttpAuthentication($http_authentication) {
    $this->http_authentication = $http_authentication;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUsePostMethod($use_post_method) {
    $this->use_post_method = $use_post_method;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisableFollowLocation($disable_follow_location) {
    $this->disable_follow_location = $disable_follow_location;
    return $this;
  }

}
