<?php

namespace Drupal\rets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining RETS Query entities.
 */
interface RetsQueryInterface extends ConfigEntityInterface {

  /**
   * Gets the RETS Server ID for this query.
   *
   * @return string
   */
  public function getRetsServer();

  /**
   * Gets the resource type for this query.
   *
   * @return string
   */
  public function getResourceType();

  /**
   * Gets the resource class for this query.
   *
   * @return string
   */
  public function getResourceClass();

  /**
   * The DMQL2 query string.
   *
   * @return string
   */
  public function getQuery();

  /**
   * Gets the count parameter for the query.
   *
   * Allowed count values are {0-2}
   *   - 0: No count is returned in the response
   *   - 1: A count record is included in the response
   *   - 2: No data is returned in the response, only the count.
   *
   * @return int
   */
  public function getCount();

  /**
   * The query type.
   *
   * The only allowed value is: 'DMQL2'
   *
   * @return string
   */
  public function getQueryType();

  /**
   * Expected RETS server reponse format.
   *
   * @return string
   */
  public function getResponseFormat();

  /**
   * Gets the limit for the number of results to return.
   *
   * @return int
   */
  public function getLimit();

  /**
   * Gets the standard names value.
   *
   * TRUE: use standard names
   * FALSE: use system names
   *
   * @return bool
   */
  public function getStandardNames();

  /**
   * Gets the destination entity.
   *
   * @return string
   *   The destination entity type to store the query data in.
   */
  public function getDestinationEntityType();

  /**
   * Gets the destination entity type (bundle).
   *
   * @return string
   *   The entity bundle to store query data in.
   */
  public function getDestinationEntityBundle();

  /**
   * Gets the field map array.
   *
   * @return array
   *   The fields mapped from RETS to our entity.
   */
  public function getFieldMap();

  /**
   * Gets the unique identifier for a resource.
   *
   * @return string
   */
  public function getKeyField();

  /**
   * Gets RETS last modified field name.
   *
   * This field represents a date when the resource was last modified. It used
   * to assist in incremental resource updates.
   *
   * @return string
   */
  public function getLastModifiedField();

  /**
   * Gets Drupal last modified field name.
   *
   * @return string
   */
  public function getDrupalLastModifiedField();

  /**
   * Gets the Drupal media image field for storing RETS media photos.
   *
   * @return string
   */
  public function getMediaImageField();

  /**
   * Gets the cron settings.
   *
   * @return array
   *   An associative array with cron settings.
   */
  public function getCron();

  /**
   * Gets the media settings.
   *
   * @return array
   *   The media settings.
   */
  public function getMedia();

  /**
   * Gets a related field from the field map.
   *
   * @param string $field
   *   The field to get from the map.
   * @param string $type
   *   The field type. Use either 'rets' or 'drupal'.
   *
   * @return string|null
   *   Returns the field or null if not found.
   */
  public function getFieldFromMap(string $field, string $type);

  /**
   * Sets the field mapping.
   */
  public function setFieldMap($map);

  /**
   * Sets the rets query string.
   */
  public function setRetsQuery($query);

}
