<?php

namespace Drupal\rets;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for RETS Query entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class RetsQueryHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    // Test route.
    if ($test_form_route = $this->getTestFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.test_form", $test_form_route);
    }
    // Data route.
    if ($data_form_route = $this->getDataFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.data_form", $data_form_route);
    }
    // Media route.
    if ($media_form_route = $this->getMediaFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.media_form", $media_form_route);
    }
    // Schedule route.
    if ($schedule_form_route = $this->getScheduleFormRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.schedule_form", $schedule_form_route);
    }
    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getEditFormRoute($entity_type);
    $route->setOption('parameters', ['rets_server' => ['type' => 'entity:rets_server']]);
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeleteFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getDeleteFormRoute($entity_type);
    $route->setOption('parameters', ['rets_server' => ['type' => 'entity:rets_server']]);
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCanonicalRoute($entity_type);
    $route->setOption('parameters', ['rets_server' => ['type' => 'entity:rets_server']]);
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getAddFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getAddFormRoute($entity_type);
    $route->setOption('parameters', ['rets_server' => ['type' => 'entity:rets_server']]);
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);
    $route->setOption('parameters', ['rets_server' => ['type' => 'entity:rets_server']]);
    return $route;
  }

  /**
   * Builds route for the test form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The test route.
   */
  public function getTestFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('test-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('test-form'));
      $operation = 'default';
      if ($entity_type->getFormClass('test')) {
        $operation = 'test';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_permission', 'administer rets queries')
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          'rets_server' => ['type' => 'entity:rets_server'],
        ]);
      return $route;
    }
  }

  /**
   * Builds route for the data form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The data route.
   */
  public function getDataFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('data-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('data-form'));
      $operation = 'default';
      if ($entity_type->getFormClass('data')) {
        $operation = 'data';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_permission', 'administer rets queries')
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          'rets_server' => ['type' => 'entity:rets_server'],
        ]);
      return $route;
    }
  }

  /**
   * Builds route for the media form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The media route.
   */
  public function getMediaFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('media-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('media-form'));
      $operation = 'default';
      if ($entity_type->getFormClass('media')) {
        $operation = 'media';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_permission', 'administer rets queries')
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          'rets_server' => ['type' => 'entity:rets_server'],
        ]);
      return $route;
    }
  }

  /**
   * Builds route for the schedule form.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The schedule route.
   */
  public function getScheduleFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('schedule-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('schedule-form'));
      $operation = 'default';
      if ($entity_type->getFormClass('schedule')) {
        $operation = 'schedule';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_permission', 'administer rets queries')
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          'rets_server' => ['type' => 'entity:rets_server'],
        ]);
      return $route;
    }
  }



}
