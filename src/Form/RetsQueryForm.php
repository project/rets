<?php

namespace Drupal\rets\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\rets\Entity\RetsQuery;

/**
 * Class RetsQueryForm.
 */
class RetsQueryForm extends RetsQueryFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\rets\Entity\RetsQueryInterface $rets_query */
    $rets_query = $this->entity;
    /** @var \Drupal\rets\Entity\RetsServerInterface $rets_server */
    $rets_server = $this->getRouteMatch()->getParameter('rets_server');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $rets_query->label(),
      '#description' => $this->t("Label for the RETS Query."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $rets_query->id(),
      '#machine_name' => [
        'exists' => '\Drupal\rets\Entity\RetsQuery::load',
      ],
      '#disabled' => !$rets_query->isNew(),
    ];

    $form['rets_server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RETS Server'),
      '#description' => $this->t('The RETS server to use for this query. You cannot edit this value.'),
      '#default_value' => $rets_server->id(),
      '#disabled' => TRUE,
    ];

    $form['resource_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RETS resource type.'),
      '#description' => $this->t('The resource type to search in this query.'),
      '#default_value' => $rets_query->getResourceType(),
      '#required' => TRUE,
    ];

    $form['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource class'),
      '#description' => $this->t('The resource class for the above resource. This should be the same as your resource type if the MLS does not have varying classes for the resource.'),
      '#default_value' => $rets_query->getResourceClass(),
      '#required' => TRUE,
    ];

    $form['key_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique key resource field.'),
      '#description' => $this->t('This is generally the ListingID for property resources, but could be another field. Use retsmd.com to view metadata for your resources if in question.'),
      '#default_value' => $rets_query->getKeyField(),
      '#required' => TRUE,
    ];

    $form['query'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Query to send to the server.'),
      '#description' => $this->t('Use the DMQL2 query language to format your search parameters. You can use relative dates in brackets {}. These values will be parsed into dates and times and formatted for RETS prior to making the query. <em>eg: DateTimeModified={TODAY-30 days}-{NOW}</em>.'),
      '#default_value' => $rets_query->getQuery(),
      '#required' => TRUE,
    ];

    $form['optional'] = [
      '#type' => 'details',
      '#title' => $this->t('Optional'),
      '#description' => $this->t('These values are optional.'),
    ];

    $form['optional']['count'] = [
      '#type' => 'select',
      '#title' => $this->t('Count'),
      '#description' => $this->t('Add a count to the response if you want to know how many records were returned.'),
      '#default_value' => $rets_query->getCount(),
      '#options' => RetsQuery::COUNTS,
    ];

    $form['optional']['query_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Query type'),
      '#description' => $this->t('Currently only DMQL2 is allowed.'),
      '#default_value' => RetsQuery::QUERY_TYPE,
      '#disabled' => TRUE,
    ];

    $form['optional']['response_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Response format'),
      '#description' => $this->t('The response format expected in this query.'),
      '#default_value' => $rets_query->getResponseFormat(),
      '#options' => RetsQuery::RESPONSE_FORMATS,
    ];

    $form['optional']['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#description' => $this->t('Limits the responses to this number of items.'),
      '#default_value' => $rets_query->getLimit(),
    ];

    $form['optional']['standard_names'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use standard names'),
      '#description' => $this->t('Check the box to use standard names, otherwise system names will be used to match fields.'),
      '#default_value' => $rets_query->getStandardNames(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $rets_query = $this->entity;
    $status = $rets_query->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label RETS Query.', [
          '%label' => $rets_query->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label RETS Query.', [
          '%label' => $rets_query->label(),
        ]));
    }
    $form_state->setRedirectUrl($rets_query->toUrl('collection'));
  }

}
