<?php

namespace Drupal\rets\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RetsServerForm.
 */
class RetsServerForm extends EntityForm {

  /**
   * RetsServerForm constructor.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Drupal messenger service.
   */
  public function __construct(Messenger $messenger) {
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\rets\Entity\RetsServerInterface $rets_server */
    $rets_server = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $rets_server->label(),
      '#description' => $this->t("Label for the RETS Server."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $rets_server->id(),
      '#machine_name' => [
        'exists' => '\Drupal\rets\Entity\RetsServer::load',
      ],
      '#disabled' => !$rets_server->isNew(),
    ];

    $form['login_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server Login URL'),
      '#description' => $this->t('The login URL to the RETS server.'),
      '#required' => TRUE,
      '#default_value' => $rets_server->getLoginUrl(),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Username to login to the RETS server.'),
      '#required' => TRUE,
      '#default_value' => $rets_server->getUsername(),
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password to the RETS server.'),
      '#description' => $this->t('Password for the RETS server.'),
      '#required' => TRUE,
      '#default_value' => $rets_server->getPassword(),
    ];

    $form['rets_version'] = [
      '#type' => 'select',
      '#options' => $rets_server->getAvailableRetsVersions(),
      '#description' => $this->t('Select a RETS standard your RETS server is configured to support.'),
      '#default_value' => $rets_server->getRetsVersion() ?? 0,
      '#required' => TRUE,
    ];

    $form['user_agent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User-agent string'),
      '#description' => $this->t('The user-agent string to pass to the RETS server.'),
      '#required' => FALSE,
      '#default_value' => $rets_server->getUserAgent(),
    ];

    $form['user_agent_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User-agent password'),
      '#description' => $this->t('The user-agent password to pass to the RETS server.'),
      '#required' => FALSE,
      '#default_value' => $rets_server->getUserAgentPassword(),
    ];

    $form['http_authentication'] = [
      '#type' => 'select',
      '#title' => $this->t('HTTP authentication method.'),
      '#description' => $this->t('Method for authenticating to RETS. Contact your MLS for the preferred method.'),
      '#required' => FALSE,
      '#options' => [
        'basic' => $this->t('Basic'),
        'digest' => $this->t('Digest'),
      ],
      '#default_value' => $rets_server->getHttpAuthentication(),
    ];

    $form['user_agent_password'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force using POST for requests.'),
      '#description' => $this->t(' Use if your MLS requires it or you have long queries which exceed 2048 characters.'),
      '#required' => FALSE,
      '#default_value' => $rets_server->getUsePostMethod(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $rets_server = $this->entity;
    $status = $rets_server->save();

    // Grab the URL of the new entity. We'll use it in the message.
    $url = $rets_server->toUrl('canonical');

    // Create an edit link.
    $edit_link = Link::fromTextAndUrl($this->t('Edit'), $url)->toString();

    if ($status == SAVED_UPDATED) {
      // If we edited an existing entity...
      $this->messenger()->addMessage($this->t('RETS Server %label has been updated.', ['%label' => $rets_server->label()]));
      $this->logger('contact')->notice('Robot %label has been updated.', ['%label' => $rets_server->label(), 'link' => $edit_link]);
    }
    else {
      // If we created a new entity...
      $this->messenger()->addMessage($this->t('RETS Server %label has been added.', ['%label' => $rets_server->label()]));
      $this->logger('contact')->notice('RETS Server %label has been added.', ['%label' => $rets_server->label(), 'link' => $edit_link]);
    }
    $form_state->setRedirectUrl($rets_server->toUrl('collection'));
  }

}
