<?php

namespace Drupal\rets\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeRepository;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rets\QueryFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RetsQueryDataForm.
 */
class RetsQueryDataForm extends RetsQueryFormBase {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepository
   */
  protected $entityTypeRepository;

  /**
   * The entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo;

  /**
   * The RETS query fetcher service.
   *
   * @var \Drupal\rets\QueryFetcher
   */
  protected $fetcher;

  /**
   * The RETS cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The Drupal entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * RetsQueryDataForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeRepository $entityTypeRepository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $bundleInfo
   *   The entity bundle info service.
   * @param \Drupal\rets\QueryFetcher $fetcher
   *   The RETS query fetcher service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The RETS cache bin.
   * @param \Drupal\Core\Entity\EntityFieldManager $fieldManager
   *   The Drupal entity field manager.
   */
  public function __construct(EntityTypeRepository $entityTypeRepository, EntityTypeBundleInfo $bundleInfo, QueryFetcher $fetcher, CacheBackendInterface $cache, EntityFieldManager $fieldManager) {
    $this->entityTypeRepository = $entityTypeRepository;
    $this->bundleInfo = $bundleInfo;
    $this->fetcher = $fetcher;
    $this->cache = $cache;
    $this->fieldManager = $fieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('rets.query_fetcher'),
      $container->get('cache.rets'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\rets\Entity\RetsQueryInterface $rets_query */
    $rets_query = $this->entity;
    $entity_type_options = [
      '0' => '- select -',
    ];
    $entities = $this->entityTypeRepository->getEntityTypeLabels(TRUE);
    // Select field for available content entities.
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination Entity'),
      '#description' => $this->t('Select the entity that you want to store query data in.'),
      '#options' => $entity_type_options + $entities['Content'],
      '#ajax' => [
        'callback' => '::entityBundleAjaxCallback',
        'wrapper' => 'entity-bundle-select-wrapper',
        'effect' => 'fade',
      ],
      '#default_value' => $rets_query->getDestinationEntityType(),
    ];

    $form['container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'entity-bundle-select-wrapper'],
    ];
    $entity_type = $form_state->getValue('entity_type') ?? $rets_query->getDestinationEntityType() ?? '';
    if (!empty($entity_type)) {
      $bundle_options = $this->bundleOptions($entity_type);
      $form['container']['entity_bundle'] = [
        '#tree' => TRUE,
        '#type' => 'select',
        '#title' => $this->t('Destination Entity Bundle'),
        '#description' => $this->t('Select the bundle of the entity type.'),
        '#options' => $bundle_options,
        '#ajax' => [
          'callback' => '::fieldMapAjaxCallback',
          'wrapper' => 'field-map-wrapper',
          'effect' => 'fade',
        ],
        '#default_value' => $rets_query->getDestinationEntityBundle(),
      ];
    }

    $entity_bundle = $form_state->getValue('entity_bundle') ?? $rets_query->getDestinationEntityBundle() ?? '';

    $form['drupal_last_modified'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal Last modified date field'),
      '#description' => $this->t('The Drupal field responsible for storing the last modified date from RETS.'),
      '#default_value' => $rets_query->getDrupalLastModifiedField(),
    ];

    $form['rets_last_modified'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RETS Last modified date field'),
      '#description' => $this->t('The RETS last modified date field.'),
      '#default_value' => $rets_query->getLastModifiedField(),
      '#state' => [
        'visible' => [
          ':input[name="entity_bundle"]' => ['filled' => TRUE],
        ],
      ],
    ];

    // Gather the number of fields in the form already.
    $num_mapped_fields = $form_state->get('num_mapped_fields');
    if ($num_mapped_fields === NULL) {
      // The form_state may not have any mapped fields yet, but check that
      // the entity has fields before assuming there are none.
      if (!empty($rets_query->getFieldMap())) {
        // Since actions are stored on the field map (we will take care of that
        // later), ensure we are only counting arrays and not the actions as
        // well.
        $field_map = $rets_query->getFieldMap();
        unset($field_map['actions']);
        $num_mapped_fields = count($field_map);
        $form_state->set('num_mapped_fields', $num_mapped_fields);
      }
      else {
        // We have to ensure that there is at least one field.
        $form_state->set('num_mapped_fields', 1);
        $num_mapped_fields = 1;
      }
    }

    $drupal_field_options = $this->buildDrupalFieldOptions($form, $form_state, $entity_type, $entity_bundle);
    $form['field_map'] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => $this->t('Field mapping'),
      '#description' => $this->t('Map the RETS data fields to your entity fields.'),
      '#prefix' => '<div id="field-map-wrapper">',
      '#suffix' => '</div>',
    ];
    if ($entity_bundle && $entity_type) {
      for ($i = 0; $i < $num_mapped_fields; $i++) {
        $form['field_map'][$i]['rets_field'] = [
          '#type' => 'select',
          '#title' => $this->t('MLS RETS field'),
          '#description' => $this->t('Select a RETS field to map it to an entity field.'),
          '#options' => $this->buildRetsFieldOptions(),
          '#default_value' => $rets_query->getFieldMap()[$i]['rets_field'] ?? '',
          '#prefix' => '<div class="form--inline clearfix">',
        ];

        $form['field_map'][$i]['drupal_field'] = [
          '#type' => 'select',
          '#title' => $this->t('Entity %entity_type field', ['%entity_type' => $entity_type]),
          '#description' => $this->t('Choose a field from the selected entity.'),
          '#default_value' => $rets_query->getFieldMap()[$i]['drupal_field'] ?? '',
          '#options' => $drupal_field_options,
          '#suffix' => '</div>',
        ];
      }

      $form['field_map']['actions'] = [
        '#type' => 'actions',
      ];
      $form['field_map']['actions']['add_field'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add one'),
        '#submit' => ['::addOne'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'field-map-wrapper',
        ],
      ];
      // If there is more than one field, add the remove button.
      if ($num_mapped_fields > 1) {
        $form['field_map']['actions']['remove_name'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove one'),
          '#submit' => ['::removeCallback'],
          '#ajax' => [
            'callback' => '::addmoreCallback',
            'wrapper' => 'field-map-wrapper',
          ],
        ];
      }
    }

    return $form;
  }

  /**
   * Builds entity bundles for select element options.
   *
   * @param string $entity_type
   *   The entity to build bundles for.
   *
   * @return array
   *   The bundle options.
   */
  public function bundleOptions($entity_type) {
    $options = [0 => '- select -'];
    $all_bundles = $this->bundleInfo->getBundleInfo($entity_type);
    foreach ($all_bundles as $bundle_id => $bundle) {
      $options[$bundle_id] = $bundle['label'];
    }
    return $options;
  }

  /**
   * Builds RETS field metadata.
   *
   * Pulls from the cache so we do not have to fetch results from the MLS
   * on every page load. In doing so, we need to provide a way to invalidate
   * the cache.
   */
  public function buildRetsFieldOptions() {
    // Cache id is in the form:
    // [rets_server id]:[rets_query id]:[form id]:field_metatdata:[module name]:[language id]
    $form_id = $this->getFormId();
    $language_id = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $server = $this->getRouteMatch()->getParameter('rets_server');
    $query = $this->getRouteMatch()->getParameter('rets_query');
    $cid = "{$server->id()}:{$query->id()}:{$form_id}:field_metadata:rets:{$language_id}";
    $rets_fields = NULL;
    if ($cache = $this->cache->get($cid)) {
      $rets_fields = unserialize($cache->data);
    }
    else {
      $metadata = $this->fetcher->getTableMetadata($server, $query);
      /** @var \PHRETS\Models\Metadata\Table $field */
      foreach ($metadata as $data) {
        $rets_fields[$data->getSystemName()] = "{$data->getSystemName()} ({$data->getMaximumLength()}, {$data->getDataType()})";
      }
      $data = serialize($rets_fields);
      $this->cache->set($cid, $data);
    }
    return $rets_fields;
  }

  /**
   * Builds Drupal field options
   */
  public function buildDrupalFieldOptions($form, FormStateInterface $form_state, $entity_type, $entity_bundle) {
    $source_fields = $this->fieldManager->getFieldDefinitions($entity_type, $entity_bundle);
    foreach ($source_fields as $source_field) {
      $fields[$source_field->getName()] = $source_field->getLabel();
    }
    // Don't allow entity keys as fields, that would break things if used.
    $definition = $this->entityTypeManager->getDefinition($entity_type);
    $keys = $definition->getKeys();
    foreach ($keys as $key) {
      unset($fields[$key]);
    }
    return $fields ?? [];
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['field_map'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $mapped_fields = $form_state->get('num_mapped_fields');
    $add_button = $mapped_fields + 1;
    $form_state->set('num_mapped_fields', $add_button);
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $mapped_fields = $form_state->get('num_mapped_fields');
    if ($mapped_fields > 1) {
      $remove_button = $mapped_fields - 1;
      $form_state->set('num_mapped_fields', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }


  /**
   * AJAX callback to build entity bundle form element.
   */
  public function entityBundleAjaxCallback($form, FormStateInterface $form_state) {
    return $form['container'];
  }

  /**
   * AJAX callback to build field map form elements.
   */
  public function fieldMapAjaxCallback($form, FormStateInterface $form_state) {
    return $form['field_map'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\rets\Entity\RetsQuery $rets_query */
    $rets_query = $this->entity;
    // Remove actions from the field_map so its not stored with the entity.
    $field_map = $rets_query->getFieldMap();
    unset($field_map['actions']);
    $rets_query->setFieldMap($field_map);
    $status = $rets_query->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Updated the %label RETS Query data settings.', [
          '%label' => $rets_query->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label RETS Query data settings.', [
          '%label' => $rets_query->label(),
        ]));
    }
    $form_state->setRedirectUrl($rets_query->toUrl('collection'));
  }

}
